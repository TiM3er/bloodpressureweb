package pl.tim3er.blood.pressure.web.dao;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class WebApplication implements CommandLineRunner {


    public static void main(String[] args) {
        SpringApplication.run(WebApplication.class, args);
    }


    @Override
    public void run(String... args) throws Exception {

    }

}
