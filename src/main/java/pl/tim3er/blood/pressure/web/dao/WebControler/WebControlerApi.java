package pl.tim3er.blood.pressure.web.dao.WebControler;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.tim3er.blood.pressure.web.dao.BloodPressureDao;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


@Controller
public class WebControlerApi {
    String endPointURL = "http://80.211.34.226:8082/bloodPressureList";
    String thisLine;

    @RequestMapping(value = "/BloodPressureList")
    public String test(Model model) {


        try {
            HttpURLConnection conn = null;
            URL u = new URL(endPointURL);
            conn = (HttpURLConnection) u.openConnection();
            conn.setRequestMethod("GET");
            conn.connect();

            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            String decodedString;
            String responseString = "";
            while ((thisLine = in.readLine()) != null) { // while loop begins here
                responseString = responseString + thisLine;
            }
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);

            List<BloodPressureDao> bloodPressureDaos = mapper.readValue(responseString, (new ArrayList<BloodPressureDao>()).getClass());
            model.addAttribute("List", bloodPressureDaos);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "index";
    }


}
