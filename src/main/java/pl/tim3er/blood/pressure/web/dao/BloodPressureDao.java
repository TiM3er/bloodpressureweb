package pl.tim3er.blood.pressure.web.dao;

import java.sql.Timestamp;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;


@ToString
@AllArgsConstructor

@NoArgsConstructor
public class BloodPressureDao {
    @JsonProperty("data")
    @JsonFormat(pattern = "yyyy-MM-ddTHH:mm:ss.SSS")
    Timestamp data;
    @JsonProperty("systiolic")
    int systiolic;
    @JsonProperty("diastiloc")
    int diastiloc;
    @JsonProperty("pulse")
    int pulse;
    @JsonProperty("medicine")
    boolean medicine;
    @JsonProperty("effort")
    int effort;


    public String getEffort() {
        if (this.effort == 1) {
            return "Mała";
        } else if (this.effort == 2) {
            return "Srednia";
        } else if (this.effort == 3) {
            return "Duza";
        }
        else {
            return "Brak danych";
        }
    }

    public Timestamp getData() {
        return data;
    }

    public int getSystiolic() {
        return systiolic;
    }

    public int getDiastiloc() {
        return diastiloc;
    }

    public int getPulse() {
        return pulse;
    }

    public boolean isMedicine() {
        return medicine;
    }
}
